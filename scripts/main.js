(() => {
  // const wsUrl = 'ws://ec2-52-29-86-167.eu-central-1.compute.amazonaws.com:5000/';
  const wsUrl = 'ws://localhost:5000/';
  const websocketDataElement = document.getElementById('websocket-data');
  let receivedString = '';

  const exampleSocket = new WebSocket(wsUrl);

  exampleSocket.onmessage = (e) => {
    const newChar = e.data;
    receivedString += newChar;

    if (newChar !== '<' && receivedString.slice(-1) !== '/') {
      websocketDataElement.innerHTML = receivedString;
    }
  };
})();
