module.exports = `
<div>
    <div>Hi, I am a developer at Godel Technologies. If you would like to tell me something, please do it via:</div>
    <div><a href="mailto:dzmitrysamsonau@gmail.com">dzmitrysamsonau@gmail.com</a></div>
    <div><a href="mailto:d.samsonau@godeltech.com">d.samsonau@godeltech.com</a></div>
    <div><a href="https://www.facebook.com/dsamsonau">facebook.com/dsamsonau</a></div>
    <div><a href="https://vk.com/demns">vk.com/demns</a></div>
    <div><a href="https://www.linkedin.com/in/dzmitrysamsonov">linkedin.com/in/dzmitrysamsonov</a></div>
    <div><a href="https://github.com/demns/samsonau.net">Link to my github</a></div>
    <div><a href="https://bitbucket.org/demns/samsonau">Link to this website on bitbucket</a></div>
    <div><a href="http://ttt.samsonau.net/">Link to the extremely simple game</a></div>
    </div>
`;
