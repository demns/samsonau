const WebSocket = require('ws');
const payload = require('./websocketsPayload');

const wss = new WebSocket.Server({ port: 5000 });

wss.on('connection', (ws) => {
  let interval;
  let i = 0;

  ws.on('close', () => {
    clearInterval(interval);
  });

  function sendItem(item) {
    ws.send(item);
  }

  interval = setInterval(() => {
    sendItem(payload[i]);
    i += 1;
    if (i >= payload.length) {
      clearInterval(interval);
    }
  }, 15);
});

console.log('started on port 8080');
