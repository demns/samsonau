const serve = require('serve');

serve(__dirname, {
  port: 80,
  ignore: ['node_modules', 'server'],
});
